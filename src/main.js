import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import axios from "axios";
import ApyTable from "./components/ApyTable";
import "./assets/styles.css";

Vue.prototype.$http = axios;

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  axios,
  ApyTable,
  render: (h) => h(App),
}).$mount("#app");
